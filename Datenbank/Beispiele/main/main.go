package main

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"test.webprogrammieren.de/Zugang"
)

func main() {
	db, err := sql.Open("mysql", Zugang.Datenbank)
	if err != nil {
		log.Fatal("Test", err, db)
	}
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}
	resultat , err := db.Exec(
		"INSERT INTO `test` (`id`, `test`) VALUES (NULL, 'Mama     zzgghghjghg und Stefft Timm und Steffi.fhfhh');",
	)
	if err !=nil {
		log.Fatal(err,resultat)
	}

}
