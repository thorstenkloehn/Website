package main
import (

	"fmt"
	"net/http"
	"github.com/gorilla/sessions"
)
var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key   = []byte("super-secret-key")
	store = sessions.NewCookieStore(key)
)

func Secret(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	// Check if user is authenticated
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "Sie haben kein zugriff", http.StatusForbidden)
		return

	}
}

func Login(w http.ResponseWriter, r *http.Request) {

	if r.FormValue("username") == "Test"{
		if r.FormValue("password") == "Test" {

			session, _ := store.Get(r, "cookie-name")
			session.Values["authenticated"] = true
			session.Save(r, w)
			fmt.Fprintf(w, `<a href="./">Weiter</a>`)

		} else {
			fmt.Fprintf(w, "Sie haben Falsch Passwort eingeben")
		}
	} else {
		fmt.Fprintf(w, "Sie haben falsche Nutzername eingeben")
	}

}
func Logout(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	// Revoke users authentication
	session.Values["authenticated"] = false
	session.Save(r, w)

}

func main()  {


}